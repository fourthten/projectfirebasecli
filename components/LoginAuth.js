// core app
import React, {Component} from 'react';
import {Platform, StyleSheet, View,Dimensions,StatusBar,AsyncStorage,Image,findNodeHandle} from 'react-native';
import {Container,Button,List,ListItem,Thumbnail,Body,Left,Right,Text} from 'native-base';
import { StackActions,NavigationActions,withNavigation } from 'react-navigation';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import { AccessToken, LoginManager } from 'react-native-fbsdk';
import { GoogleSignin } from 'react-native-google-signin';
import firebase from 'react-native-firebase';

class Login extends Component {
  static navigationOptions = {title:'Login',header:null}

  constructor(props){
    super(props);
    this.db = firebase.firestore();
    this.db.settings.areTimestampsInSnapshotsEnabled = true;
    this.state={userdata:null,viewRef:0};
  }
  facebookLogin = async () => {
    try {
      const result = await LoginManager.logInWithReadPermissions(['public_profile', 'email']);

      if (result.isCancelled) {
        throw new Error('User cancelled request'); // Handle this however fits the flow of your app
      }

      console.log(`Login success with permissions: ${result.grantedPermissions.toString()}`);

      // get the access token
      const data = await AccessToken.getCurrentAccessToken();

      if (!data) {
        throw new Error('Something went wrong obtaining the users access token'); // Handle this however fits the flow of your app
      }

      // create a new firebase credential with the token
      const credential = firebase.auth.FacebookAuthProvider.credential(data.accessToken);

      // login with credential
      const currentUser = await firebase.auth().signInWithCredential(credential);
      if(currentUser){
        this.setState({user:currentUser});
        const storage = await AsyncStorage.setItem('userdata',JSON.stringify(currentUser));
        var user = currentUser.user.toJSON();
        this.db.collection("users").doc(user.uid).collection("profile").doc("user").set(user);
        // this.db.collection("users").doc(user.uid).collection("rating").add({rating:0,ratedby:"System"});
        this.reroute();
      }
    } catch (e) {
      alert(e.toString());
    }
  }

    googleLogin = async () => {
      try {
// Add any configuration settings here:
        await GoogleSignin.configure({
          webClientId:"575832393950-v4ap7a8k8h26d15rtm9fl56l8dm1sar4.apps.googleusercontent.com"
        });

        const data = await GoogleSignin.signIn();

        // create a new firebase credential with the token
        const credential = firebase.auth.GoogleAuthProvider.credential(data.idToken, data.accessToken)
        // login with credential
        const currentUser = await firebase.auth().signInWithCredential(credential);
        if(currentUser){
          this.setState({user:currentUser});
          const storage = await AsyncStorage.setItem('userdata',JSON.stringify(currentUser));
          var user = currentUser.user.toJSON();
          this.db.collection("users").doc(user.uid).collection("profile").doc("user").set(user);
          this.reroute();
        }
      } catch (e) {
          console.log(e);
          alert(e.toString());
        }
      }

  async reroute(){
    const resetAction = StackActions.reset({
      index:0,
      actions:[
        NavigationActions.navigate({routeName:'ListPage'})
      ]
    });
      await this.props.navigation.dispatch(resetAction);
  }

  render(){
    return(
      <Container style={{flex:1,flexDirection: 'row',alignItems: 'center',alignContent: 'center',justifyContent: 'center',fontFamily: 'SourceSansPro-Regular'}}>
        <View style={{flex:1,flexDirection: 'column',alignContent: 'center',justifyContent: 'center',alignItems: 'center'}}>
          <View style={{flex:1,flexDirection: 'column',alignSelf: 'center',alignItems: 'center',justifyContent: 'center',borderRadius: 40}}>
            <Text style={{fontSize: 14}}>Masuk menggunakan:</Text>
            <Button rounded  block style={{flexDirection: 'column',borderRadius: 40}} onPress={this.facebookLogin}>
              <Left>
                <MaterialCommunityIcons name="facebook" style={{padding: 10,fontSize: 18,top:0,color: 'white',alignSelf: 'center'}}/>
              </Left>
            </Button>
            <Button rounded  block style={{flexDirection: 'column',borderRadius: 40}} danger onPress={this.googleLogin}>
              <Left>
                <MaterialCommunityIcons name="google" style={{padding: 10,fontSize: 18,top:0,color: 'white',alignSelf: 'center'}}/>
              </Left>
            </Button>
          </View>
        </View>
      </Container>
    );
  }
}


export default withNavigation(Login);
