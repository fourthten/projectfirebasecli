import React, {Component} from 'react';
import { AppRegistry, View, Platform } from 'react-native';
import { Item, Input, Button, Text, Icon } from 'native-base';
import firebase from 'react-native-firebase';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import { withNavigation } from 'react-navigation';

class JustText extends Component{
  constructor(props) {
  	super(props);
  	this.state = {
      name: '',
      user: null
  	};
    this.db = firebase.firestore();
    this.db.settings.areTimestaInSnapshotsEnabled  = true;
  }
  componentDidMount(){
    this.db.collection("todoTasks").where("status", "==", "completed")
    .onSnapshot({ includeMetadataChanges: true },(querySnapshot) => {
      console.log("Query snapshot:",querySnapshot);
        querySnapshot.forEach((doc) => {
          var data = doc.data();
          console.log("data:",data);
            var source = querySnapshot.metadata.fromCache ? "local cache" : "server";
            // var writes = querySnapshot.metadata.hasPendingWrites ? "cache" : "server";
            console.log("Data came from " + source );
        });
    });
  }
  render(){
    const { navigation } = this.props;
    const nametext = navigation.getParam('name');
    return(
      <View
				style={{
          padding: 15,
					backgroundColor: 'white',
					borderRadius: Platform.OS === 'android' ? 0 : 30
				}}
      >
        <Text>read {nametext}</Text>
      </View>
    );
  }
}

export default withNavigation(JustText);
