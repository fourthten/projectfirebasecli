import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View, Dimensions, StatusBar, TouchableOpacity, ImageBackground, AsyncStorage} from 'react-native';
import {Container, StyleProvider, Header, Body, Left, Right, Button, Thumbnail, Item, Input} from 'native-base';
import {NavigationActions, StackActions, withNavigation} from 'react-navigation';
import getTheme from '../native-base-theme/components';
import material from '../native-base-theme/variables/material';
const w = Dimensions.get("window").width;
const h = Dimensions.get("window").height;

class ViewTriangle extends Component {
  static navigationOptions = {title:'Login', header:null}

  constructor(props){
    super(props);
    this.state = {
      userdata: null,
      username: '',
      users: []
    };
  }

  Login(){
    // this.props.navigation.navigate("Home");
    if (this.state.username.trim()===""){
      console.log("kosong");
      return;
    }
    this.setState(prevState => {
      console.log("prevState", prevState);
      return{
        users: prevState.users.concat(prevState.username)
      }
    })
  }

  render() {
    console.log("username", this.state.username);
    console.log("user", this.state.users);
    return (
      <StyleProvider style={getTheme(material)}>
        {/*<ImageBackground source={require("../assets/images/background_app.png")} style={{width: w, height: h, alignItems: 'center', justifyContent: 'center'}}>*/}
        <View style={{backgroundColor: 'rgb(250, 255, 29)', width: w, height: h, alignItems: 'center', justifyContent: 'center'}}>
          <View style={styles.bgtriangletop}></View>
          <View style={styles.bgtrianglebot}></View>
          <StatusBar barStyle="dark-content" backgroundColor="rgb(250, 255, 29)" hidden={false}/>
          <View opacity={0.85} style={styles.viewlogin}>
            <Item rounded style={{width: w*0.75, height: h*0.07, borderColor: 'rgb(250, 255, 29)'}}>
              <Input placeholder='Username' onChangeText={(val)=>{this.setState({username: val})}} placeholderTextColor='rgba(250, 255, 29, 0.7)' style={{left: 10, color: 'rgb(250, 255, 29)'}}/>
            </Item>
            <Item rounded style={{width: w*0.75, height: h*0.07, borderColor: 'rgb(250, 255, 29)'}}>
              <Input secureTextEntry={true} placeholder='Password' placeholderTextColor='rgba(250, 255, 29, 0.7)' style={{left: 10, color: 'rgb(250, 255, 29)'}}/>
            </Item>
            <Button onPress={()=>{this.Login()}} style={{width: w*0.75, height: h*0.07, borderRadius: 20, alignSelf: 'center', alignItems: 'center', justifyContent: 'center', backgroundColor: 'rgb(250, 255, 29)'}}>
              <Text style={{fontSize: 18, color: '#000', fontWeight: 'bold'}}>Masuk</Text>
            </Button>
          </View>
        </View>
      </StyleProvider>
    );
  }
}

const styles = StyleSheet.create({
  viewlogin: {
    height: h*0.3,
    width: w*0.9,
    flexDirection: 'column',
    alignContent: 'space-around',
    justifyContent: 'space-around',
    alignItems: 'center',
    paddingVertical: 10,
    borderRadius: 25,
    borderWidth: 5,
    borderColor: 'rgb(250, 255, 29)',
    backgroundColor: 'rgb(0, 0, 0)',
    elevation: 10,
    zIndex: 2
  },
  bgtriangletop: {
    position: 'absolute',
    top:0,
    right:0,
    width: w*0,
    height: h*0,
    backgroundColor: 'transparent',
    borderStyle: 'solid',
    borderRightWidth: w*0.6,
    borderTopWidth: h*0.5,
    borderRightColor: 'rgb(0, 0, 0)',
    borderTopColor: 'transparent',
    zIndex: 1
  },
  bgtrianglebot: {
    position: 'absolute',
    bottom:0,
    right:0,
    width: w*0,
    height: h*0,
    backgroundColor: 'transparent',
    borderStyle: 'solid',
    borderLeftWidth: w*0.6,
    borderTopWidth: h*0.5,
    borderLeftColor: 'transparent',
    borderTopColor: 'rgb(0, 0, 0)',
    zIndex: 1
  }
});

export default withNavigation(ViewTriangle);
