import React, {Component} from 'react';
import { AppRegistry, View, Platform, AsyncStorage } from 'react-native';
import { Item, Input, Button, Text, Icon } from 'native-base';
import firebase from 'react-native-firebase';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import { withNavigation } from 'react-navigation';
// import { AccessToken, LoginManager } from 'react-native-fbsdk';
import { GoogleSignin } from 'react-native-google-signin';

class LoginComponent extends Component {
  static navigationOptions = {
    title: 'LOGIN!',
  };
  constructor(props) {
  	super(props);
    this.unsubscriber = null;
  	this.state = {
  		isAuthenticated: false,
      typedEmail: '',
      typedPassword: '',
      user: null,
      isSignedIn: false
  	};
  }

  componentDidMount(){
  	this.unscubscriber = firebase.auth().onAuthStateChanged((changedUser) => {
  		this.setState({ user: changedUser });
  	})
  }
  componentWillUnmount(){
  	if (this.unsubscriber){
  		this.unsubscriber();
  	}
  }
  onAnonymousLogin = () => {
	firebase.auth().signInAnonymously()
		.then(() => {
			console.log('Login succesfully');
			this.setState({
				isAuthenticated: true
			});
		})
		.catch((error) => {
			console.log(`Login failed. Error = ${error} `);
		});
	}
  onRegister = () => {
  	firebase.auth().createUserWithEmailAndPassword(this.state.typedEmail, this.state.typedPassword)
    .then((loggedInUser) => {
  	   this.setState({user: loggedInUser})
       console.log(`Register With User : ${JSON.stringify(loggedInUser.toJSON())}`);
    })
    .catch((error) => {
    	console.log(`Register fail with error: ${error}`);
    });
  }
  onLogin = () => {
  	firebase.auth().signInWithEmailAndPassword(this.state.typedEmail, this.state.typedPassword)
    .then((loggedInUser) => {
      console.log(loggedInUser.user);
      this.props.navigation.navigate('ListPage');
    })
    .catch((error) => {
    	console.log(`Login fail with error: ${error}`);
    });
  }
  ontodo = () => {
    this.props.navigation.navigate('ListPage');
  }

  onGoogleLogin = async () => {
    try {
// Add any configuration settings here:
      await GoogleSignin.configure({
        webClientId:"575832393950-v4ap7a8k8h26d15rtm9fl56l8dm1sar4.apps.googleusercontent.com"
      });

      const data = await GoogleSignin.signIn();

      // create a new firebase credential with the token
      const credential = firebase.auth.GoogleAuthProvider.credential(data.idToken, data.accessToken)
      // login with credential
      const currentUser = await firebase.auth().signInAndRetrieveDataWithCredential(credential);
      if(currentUser){
        this.setState({user:currentUser});
        const storage = await AsyncStorage.setItem('userdata',JSON.stringify(currentUser));
        var user = currentUser.user.toJSON();
        // this.db.collection("users").doc(user.uid).collection("profile").doc("user").set(user);
        // this.db.collection("users").doc(user.uid).collection("rating").add({rating:0,ratedby:"System"});
        this.reroute();
      }
    } catch (e) {
        console.log(e);
        alert(e.toString());
    }
  }
  async reroute(){
    const resetAction = StackActions.reset({
      index:0,
      actions:[
        NavigationActions.navigate({routeName:'ListPage'})
      ]
    });
      await this.props.navigation.dispatch(resetAction);
  }



  render(){
		return(
			<View
				style={{
          padding: 15,
					backgroundColor: 'white',
					borderRadius: Platform.OS === 'android' ? 0 : 30
				}}
      >
				<Text style={{
          fontSize: 20,
          fontWeight: 'bold',
          textAlign: 'center',
          margin: 40
          }}
        >Login with Firebase </Text>
        <Button block containerStyle={{
          padding: 10,
          borderRadius: 4,
          backgroundColor: 'rgb(226,161,184)',
          textAlign: 'center'
          }}
          style={{ fontSize: 18, color: 'white' }}
          onPress={this.onAnonymousLogin}
        >
          <Icon name='home' />
          <Text>Login Anonymous</Text></Button>
        <Text style={{ margin:20, fontSize:15 }}>{this.state.isAuthenticated == true ? 'Logged in anonymous' : ''}</Text>
        <Item>
          <Input
            keyboardType='email-address'
            placeholder='Enter your email'
            autoCapitalize='none'
            onChangeText={
              (text) => {
                this.setState({ typedEmail: text });
              }
            }
          />
        </Item>
        <Item>
          <Input
            keyboardType='default'
            placeholder='Enter your password'
            secureTextEntry={true}
            onChangeText={
              (text) => {
                this.setState({ typedPassword: text });
              }
            }
          />
        </Item>
        <Button block rounded success
          style={{ marginTop: 15 }}
          onPress={this.onLogin}
        >
          <MaterialCommunityIcons name='login' />
          <Text>Login</Text>
        </Button>
        <Button block rounded info
          style={{ marginTop: 15 }}
          onPress={this.onRegister}
        >
          <Text>Register</Text>
        </Button>
        <Button block containerStyle={{
          padding: 10,
          borderRadius: 4,
          backgroundColor: 'rgb(226,161,184)',
          textAlign: 'center'
          }}
          style={{ fontSize: 18, color: 'white', marginVertical: 20 }}
          onPress={this.onGoogleLogin}
        >
          <MaterialCommunityIcons name='google' />
          <Text>Login Google</Text>
        </Button>
      </View>
    );
  }
}



export default withNavigation(LoginComponent);
