import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View, Dimensions, StatusBar, TouchableOpacity, ImageBackground, AsyncStorage} from 'react-native';
import {Container, StyleProvider, Header, Body, Left, Right, Button, Thumbnail, Item, Input} from 'native-base';
import {NavigationActions, StackActions, withNavigation} from 'react-navigation';
import getTheme from '../native-base-theme/components';
import material from '../native-base-theme/variables/material';
const w = Dimensions.get("window").width;
const h = Dimensions.get("window").height;

class ViewWave extends Component{
  constructor(props) {
  	super(props);
  	this.state = {
      name: '',
      user: null
  	};
  }
  componentDidMount(){
  }
  render(){
    return(
      <View
				style={
          styles.container
				}
      >
        <View style={styles.oval}/>
        <View style={styles.oval2}/>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  square:{
    width:100,
    height:180,
    backgroundColor:'red',
    position:'absolute',
    top:160

  },
    oval: {
        position:'relative',
        marginTop: 10,
        marginLeft: w*0.15,
        width: 100,
        height: 200,
        borderRadius: 50,
        backgroundColor: 'red',
        //borderWidth:2,
        //borderColor:'black',
        transform: [
          {scaleX: 2}
        ]
    },
    oval2: {
        position:'relative',
        width: 100,
        height: 200,
        marginTop: h*0.3,
        marginLeft: w*0.15,
        borderRadius: 50,
        backgroundColor: 'red',
        //borderWidth:2,
        //borderColor:'black',
        transform: [
          {scaleX: 2}, {scaleY: 3}
        ]
    },
    container:{
      flex:1,
      alignItems:'flex-start',
      justifyContent:'flex-start',
      flexDirection: 'row'
    }
});

export default withNavigation(ViewWave);
