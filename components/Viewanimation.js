import React, {Component} from 'react';
import { AppRegistry, View, Platform, Animated, TouchableOpacity, StyleSheet, Easing, Dimensions, Image } from 'react-native';
import { Item, Input, Button, Text, Icon } from 'native-base';
import firebase from 'react-native-firebase';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import { withNavigation } from 'react-navigation';

var { width, height } = Dimensions.get('window');
class Viewanimation extends Component{
  constructor(props) {
  	super(props);
  	this.state = {
      name: '',
      user: null,
      fadeValue: new Animated.Value(0),
      xValue: new Animated.Value(0),
      springValue: new Animated.Value(0.5)
  	};
    this.db = firebase.firestore();
    this.db.settings.areTimestaInSnapshotsEnabled  = true;
  }
  componentDidMount(){
  }
  Fadeanimation = () => {
    Animated.timing(this.state.fadeValue,{
      toValue: 1,
      duration: 1200
    }).start();
  }
  Moveanimation = () => {
    Animated.timing(this.state.xValue,{
      toValue: width - 120,
      duration: 1500,
      // easing: Easing.linear
      easing: Easing.back()
      // easing: Easing.cubic
    }).start(() => {
      Animated.timing(this.state.xValue,{
        toValue: 0,
        duration: 1200,
        easing: Easing.back()
      }).start(() => {
        this.Moveanimation();
      })
    });
  }
  Springanimation = () => {
    Animated.spring(this.state.springValue, {
      toValue: 1.5,
      friction: 1
    }).start();
  }
  render(){
    const { navigate } = this.props.navigation;
    return(
      <View style={styles.container}>
        <Animated.View style={[styles.animationView,
          // {opacity: this.state.fadeValue}
          {left: this.state.xValue}
        ]}>
        </Animated.View>
        {/*}<Animated.Image
          source={require('../assets/images/IGNISMART.png')}
          style={[
            styles.imageView,
            // {left: this.state.xValue}
            {transform:[{scale: this.state.springValue}], alignSelf: "center"}
          ]}>
        </Animated.Image>*/}
        <TouchableOpacity style={styles.button}
          // onPress={this.Fadeanimation}
          onPress={this.Moveanimation}
          // onPress={this.Springanimation}
        >
          <Text style={styles.buttonText}>Animate</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container:{
    padding: 15,
    backgroundColor: 'white',
    borderRadius: Platform.OS === 'android' ? 0 : 30,
    flex: 1,
    justifyContent: "center",
    // alignItems: "center"
  },
  animationView: {
    width: 100,
    height: 100,
    backgroundColor: "red"
  },
  button: {
    backgroundColor: "blue",
    height: 45,
    marginTop: 20,
    alignSelf: "center"
  },
  buttonText: {
    color: "white",
    padding: 12,
    paddingHorizontal: 20,
    fontWeight: "bold",
    fontSize: 18
  },
  imageView: {
    width: 100,
    height: 100,
    backgroundColor: 'transparent'
  }
})

export default withNavigation(Viewanimation);
