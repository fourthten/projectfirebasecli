import React, {Component} from 'react';
import {AppRegistry, FlatList, StyleSheet, View, Image, Alert, Platform,TouchableOpacity, TouchableHighLight, Dimensions, TextInput, AsyncStorage} from 'react-native';
import { Container, Header, Content, Form, Item, Input, Label, Button, Text, Icon,List,ListItem, Left, Right, SwipeRow } from 'native-base';
import firebase from 'react-native-firebase';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import {  StackActions,NavigationActions,withNavigation  } from 'react-navigation';
import Dialog from "react-native-dialog";
import { GoogleSignin } from 'react-native-google-signin';

class TodoList extends Component {
  static navigationOptions = {
    title: 'ToDoList',
  };
  db = null;
  constructor(props) {
  	super(props);
    this.state = ({
      todoTasks: [],
      newTaskName: '',
      dialogVisible: false,
      updateTaskName: '',
      idkey: '',
      loading: false
    });
    this.db = firebase.firestore();
    this.ref = firebase.firestore().collection('todoTasks');
    this.db.settings.areTimestaInSnapshotsEnabled  = true;
  }
  componentDidMount(){
  	this.db.collection("todoTasks").onSnapshot((querySnapshot) => {
  		const todos = [];
  		querySnapshot.forEach((doc) => {
        var data = doc.data();
        console.log("Data:",data);
        if(data.status != "deleted"){
          todos.push({
    				taskName: doc.data().taskName,
            taskStatus: doc.data().status,
            key: doc.id
    			});
        }
  		});
  		this.setState({
  			todoTasks: todos.sort((a, b) => {
  				return (a.taskName < b.taskName);
  			}),
  			loading: false,
  		});
  	});
  }
  onPressAdd = () => {
  	if(this.state.newTaskName.trim() === ''){
  		alert('task name is blank');
  		return;
  	}
  	this.ref.add({
  		taskName: this.state.newTaskName,
      status:"new"
  	}).then((data) => {
  		console.log(`added data = ${data}`);
  		this.setState({
  			newTaskName: '',
  			loading: true
  		});
  	}).catch((error) => {
  		console.log(`error adding Firestore document = ${error}`);
  		this.setState({
  			newTaskName: '',
  			loading: true
  		});
  	});
  }
  onPressDelete = (params) => {
  	this.ref.doc(params).update({
      status:"deleted"
    }).then((data) => {
  		console.log(`deleted data = ${data}`);
      setTimeout(()=>{
        this.setState({
    			newTaskName: '',
    			loading: true
    		});
      },400);

  	}).catch((error) => {
  		console.log(`error deleting Firestore document = ${error}`);
  		this.setState({
  			newTaskName: '',
  			loading: true
  		});
  	});
  }
  onPressChecked = (params) => {
  	this.ref.doc(params).update({
      status:"completed"
    }).then((data) => {
  		console.log(`completed data = ${data}`);
      setTimeout(()=>{
        this.setState({
    			newTaskName: '',
    			loading: true
    		});
      },400);

  	}).catch((error) => {
  		console.log(`error completing Firestore document = ${error}`);
  		this.setState({
  			newTaskName: '',
  			loading: true
  		});
  	});
  }
  onPressUpdate = (params) => {
    if(this.state.updateTaskName.trim() === ''){
  		alert('task name is blank');
  		return;
  	}
  	this.ref.doc(params).update({
  		taskName: this.state.updateTaskName
  	}).then((data) => {
  		console.log(`updated data = ${data}`);
  		this.setState({
  			updateTaskName: '',
        dialogVisible: false,
  			loading: true
  		});
  	}).catch((error) => {
  		console.log(`error editing Firestore document = ${error}`);
  		this.setState({
  			updateTaskName: '',
        dialogVisible: false,
  			loading: true
  		});
  	});
  }
  onPressShow = (params, task) => {
    this.setState({ dialogVisible: true, updateTaskName: task, idkey: params });
  };
  onPressCancel = () => {
    this.setState({ dialogVisible: false });
  };

  // logout
    onLogout = async () => {
      await AsyncStorage.removeItem("userdata").then(()=>{
        firebase.auth().signOut().then(()=>{
          this.reroute();
        }).catch(err=>{
          console.log(err.message);
        })
      }).catch((error)=>{
        console.log("error logout",error.message);
      })
    }

    async reroute(){
      const resetAction = StackActions.reset({
        index:0,
        actions:[
          NavigationActions.navigate({routeName:'Login'})
        ]
      });
        await this.props.navigation.dispatch(resetAction);
    }
  render(){
    const { navigate } = this.props.navigation;
    return(
      <View
        style={{
          padding: 15,
          backgroundColor: 'white',
          marginTop: Platform.OS === 'android' ? 0 : 34
        }}
      >
        <Button info
          style={{ marginLeft: 10, paddingHorizontal: 100 }}
          onPress={this.onLogout}
        >
          <MaterialCommunityIcons name='logout' />
          <Text>LogOut</Text>
        </Button>
        <View style={{
          padding: 15,
          flexDirection: 'row',
          alignItems: 'center'
        }}>
          <Item style={{ width:'90%' }}>
            <Input
              keyboardType='default'
              placeholder='Enter task name'
              autoCapitalize='none'
              value={this.state.newTaskName}
              onChangeText={
                (text) => {
                  this.setState({ newTaskName: text });
                }
              }
            />
          </Item>
          <Button block info
            style={{ marginLeft: 10, paddingHorizontal: 10 }}
            onPress={this.onPressAdd}
          >
            <MaterialCommunityIcons name='login' />
          </Button>
        </View>
        <List>
        {this.state.todoTasks.map((data,index)=>{
          if(data.taskStatus == "new"){
            return(
              <SwipeRow key={data.key}
                leftOpenValue={75}
                rightOpenValue={-150}
                left={
                  <Button success onPress={()=>{this.onPressChecked(data.key)}}>
                    <FontAwesome style={{fontSize:25}} active name="check" />
                  </Button>
                }
                body={
                  <View>
                    <Text>{data.taskName}</Text>
                  </View>
                }
                right={
                  <View style={{ flexDirection: 'row' }}>
                    <Button info onPress={()=>{this.onPressShow(data.key, data.taskName)}}>
                      <FontAwesome style={{fontSize:25}} active name="edit" />
                    </Button>
                    <Button danger onPress={()=>{this.onPressDelete(data.key)}}>
                      <Icon active name="trash" />
                    </Button>
                  </View>
                }
              />
            );
          } else {
            return (
              <Button
                key={data.key}
                transparent
                onPress={() =>
                  navigate('TextPage', { name: data.taskName })
                }
              >
                <Text style={{ paddingVertical:10, color: 'green' }}>{data.taskName}</Text>
              </Button>
            );
          }
        })}
        </List>
        <Dialog.Container visible={this.state.dialogVisible}>
          <Dialog.Title>Update Task</Dialog.Title>
          <Dialog.Description>
            Put your New Task Name in column.
          </Dialog.Description>
          <Dialog.Input
            keyboardType='default'
            placeholder='Enter task name'
            autoCapitalize='none'
            value={this.state.updateTaskName}
            onChangeText={
              (text) => {
                this.setState({ updateTaskName: text });
              }
            }
          />
          <Dialog.Button label="Update" onPress={()=>{this.onPressUpdate(this.state.idkey)}}/>
          <Dialog.Button label="Cancel" onPress={this.onPressCancel}/>
        </Dialog.Container>
      </View>
    );
  }
}

export default withNavigation(TodoList);
