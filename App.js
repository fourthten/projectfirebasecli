/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View, TextInput, Button, FlatList, AsyncStorage} from 'react-native';
import firebase from 'react-native-firebase';
import TodoList from './components/TodoList';
import LoginComponent from './components/LoginComponent';
import JustText from './components/JustText';
import LoginAuth from './components/LoginAuth';
import Viewanimation from './components/Viewanimation';
import ViewTriangle from './components/ViewTriangle';
import ViewWave from './components/ViewWave';

import { Root, Container } from  'native-base';
import { createStackNavigator } from 'react-navigation';

type Props = {};

const AppRoute = createStackNavigator({
  Login:{screen :LoginAuth},
  ListPage : {screen :TodoList},
  TextPage : {screen: JustText},
  Viewpage : {screen: Viewanimation},
  ViewTriangle : {screen: ViewTriangle},
  ViewWave : {screen: ViewWave}
},{initialRouteName:'ViewTriangle'});
const AuthRoute = createStackNavigator({
  Login:{screen :LoginAuth},
  ListPage : {screen :TodoList},
  TextPage : {screen: JustText},
  Viewpage : {screen: Viewanimation},
  ViewTriangle : {screen: ViewTriangle},
  ViewWave : {screen: ViewWave}
},{initialRouteName:'Login'});

export default class App extends Component<Props> {
  constructor(props){
    super(props);
    this.state = {userdata:null};
  }

  async componentDidMount(){
    await AsyncStorage.getItem("userdata").then((user)=>{
      if(user == null){
        setTimeout(()=>{
          this.setState({userdata:false});
        },2000);
        console.log("user",user);
      }
      else{
        setTimeout(()=>{
          this.setState({userdata:JSON.parse(user)});
        },2000);
        console.log("user",user);
      }
    });
  }

  render(){
    if(this.state.userdata == false){
      return(
        <Root>
          <AuthRoute />
        </Root>
      );
    } else {
      return(
        <Root>
          <AppRoute />
        </Root>
      );
    }
  }
}

const styles = StyleSheet.create({
  container: {
	  alignItems: 'center',
    backgroundColor: 'white'
  }
});
